
// MapCompareView.cpp : MapCompareView 类的实现
//

#include "stdafx.h"
#include "MapCompare.h"
#include "MapCompareView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <imm.h>
#pragma comment (lib, "imm32.lib")

HIMC g_hImc;
void DisableIME(HWND hWnd)
{
    g_hImc = ImmGetContext(hWnd);
    if (g_hImc) {
        ImmAssociateContext(hWnd, NULL);
	}
    ImmReleaseContext(hWnd, g_hImc);
    ::SetFocus(hWnd);
}

///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
//显示用户区的所有信息
void MapCompareView::displaymap(void)
{
	//x；y：地图区域中当前编辑位置的坐标，值域（0~199）
	m_LeftBuffDC.BitBlt(0,0,EDIT_AREA_W,EDIT_AREA_H,&m_TileDC,0,0,BLACKNESS);//地图区域使用黑色
	m_RightBuffDC.BitBlt(0,0,EDIT_AREA_W,EDIT_AREA_H,&m_TileDC,0,0,BLACKNESS);//地图区域使用黑色

	//（在游戏中地图外面的区域使用黑色）
	draw_edit_area();//显示地图区域长21格，高21格。
	draw_table_lines();//在主缓上绘制分界线

	CDC *pdc = GetDC();
	pdc->SelectObject(m_ViewFont);
	pdc->BitBlt(0,0,EDIT_AREA_W,EDIT_AREA_H,&m_LeftBuffDC,0,0,SRCCOPY);//将主缓送入屏幕
	pdc->BitBlt(RIGHT_EDIT_AREA_X_OFFSET,
		0,EDIT_AREA_W,EDIT_AREA_H,&m_RightBuffDC,0,0,SRCCOPY);//将主缓送入屏幕

	//绘制鼠标的圈选范围
	if (m_rcMouseSelectArea.Width() > 0 &&
		m_rcMouseSelectArea.Height() > 0) {
		CPen greenPen;//绿色画笔
		greenPen.CreatePen(PS_SOLID,1,RGB(0,255,0));
		CPen* pOldPen = pdc->SelectObject(&greenPen);
		drawrect(&m_rcMouseSelectArea, pdc);
		pdc->SelectObject(pOldPen);//恢复画笔
	}

	//修改状态栏的显示内容
	pdc->BitBlt(0,INFO_BAR_Y,600,20,pdc,0,0,WHITENESS);
	CString sEditInfo;
	sEditInfo.Format("编辑区左上角的位置： x= %d    y= %d"\
		"       选择区左上角 left= %d    top= %d    右下角 right= %d    bottom= %d",
		m_ptAreaTopLeft.x, m_ptAreaTopLeft.y,
		m_rcSelectedGridArea.left,
		m_rcSelectedGridArea.top,
		m_rcSelectedGridArea.right,
		m_rcSelectedGridArea.bottom);
	pdc->TextOut(10, INFO_BAR_Y, sEditInfo);

	//刷新滚动条显示
	m_scoLeftAreaH.Invalidate();
	m_scoLeftAreaV.Invalidate();
	m_scoRightAreaH.Invalidate();
	m_scoRightAreaV.Invalidate();
	ReleaseDC(pdc);
}

void MapCompareView::GetGridScale(int* scaled_w, int* scaled_h)
{
	switch (m_nMapScale) {
	case SCALE1:
		*scaled_w = TILE_W;
		*scaled_h = TILE_H;
		break;
	case SCALE2:
		*scaled_w = TILE_W / 2;
		*scaled_h = TILE_H / 2;
		break;
	case SCALE4:
		*scaled_w = TILE_W / 4;
		*scaled_h = TILE_H / 4;
		break;
	case SCALE8:
		*scaled_w = TILE_W / 8;
		*scaled_h = TILE_H / 8;
		break;
	}
}

//显示地图的一个方格
void MapCompareView::draw_map_ceil(int i, int x, int y, int w, int h, CDC *pbuffDC)
{
	if ((i <= MAP_TILES_NUM_MAX) && (i > 0)) {
		int sx = (((i - 1) % MAP_TILES_LOGIC_W) * TILE_W);
		int sy = (((i - 1) / MAP_TILES_LOGIC_W) * TILE_H);//i的范围从0到800
		pbuffDC->BitBlt(x,y,w,h,&m_TileDC,sx,sy,SRCCOPY);//绘制地图
	}
	else {
		pbuffDC->BitBlt(x,y,w,h,&m_TileDC,0,0,BLACKNESS);//黑色
	}
}

//显示画面上所有的分界线
void MapCompareView::draw_table_lines()
{
	CPen* pOldPenLeft = NULL;
	CPen* pOldPenRight = NULL;
	CPen whitePenLeft;//白色画笔
	CPen whitePenRight;
	CPen redPenLeft;//红色画笔
	CPen redPenRight;
	whitePenLeft.CreatePen(PS_SOLID,1,RGB(255,255,255));
	whitePenRight.CreatePen(PS_SOLID,1,RGB(255,255,255));
	redPenLeft.CreatePen(PS_SOLID,1,RGB(255,0,0));
	redPenRight.CreatePen(PS_SOLID,1,RGB(255,0,0));

	int scale_w = TILE_W;
	int scale_h = TILE_H;
	GetGridScale(&scale_w, &scale_h);

	int a = m_ptAreaTopLeft.x;//左上角的格子在地图矩阵中的坐标
	int b = m_ptAreaTopLeft.y;
	int x = 0;
	int y = 0;
	int m, n;

	int w_size = EDIT_AREA_W / scale_w;
	int h_size = EDIT_AREA_W / scale_h;
	CRect rcSelected = m_rcSelectedGridArea;
	rcSelected.right++;
	rcSelected.bottom++;

	for (m = 0; m < h_size; m++)
	{
		for (n = 0; n < w_size; n++)
		{
			if ((a < MAP_GRID_W) &&
				(a >= 0)         &&
				(b < MAP_GRID_H) &&
				(b >= 0))
			{
				CPoint pt(a, b);
				if (PtInRect(&rcSelected, pt)) {
					pOldPenLeft = m_LeftBuffDC.SelectObject(&redPenLeft);
					pOldPenRight = m_RightBuffDC.SelectObject(&redPenRight);
					CRect rc(x, y, x+scale_w, y+scale_h);
					drawrect(&rc, &m_LeftBuffDC);
					drawrect(&rc, &m_RightBuffDC);
					if (SCALE1 == m_nMapScale) {
						rc.left+=1;//缩小一圈，画双线
						rc.top+=1;
						rc.right-=1;
						rc.bottom-=1;
						drawrect(&rc, &m_LeftBuffDC);
						drawrect(&rc, &m_RightBuffDC);
					}
					m_LeftBuffDC.SelectObject(pOldPenLeft);
					m_RightBuffDC.SelectObject(pOldPenRight);
				}
				else if (map_diff[a][b]) {
					pOldPenLeft = m_LeftBuffDC.SelectObject(&whitePenLeft);
					pOldPenRight = m_RightBuffDC.SelectObject(&whitePenRight);
					CRect rc(x, y, x+scale_w, y+scale_h);
					drawrect(&rc, &m_LeftBuffDC);
					drawrect(&rc, &m_RightBuffDC);
					m_LeftBuffDC.SelectObject(pOldPenLeft);
					m_RightBuffDC.SelectObject(pOldPenRight);
				}
			}
			a ++;
			x += scale_w;
		}
		a = m_ptAreaTopLeft.x;
		b ++;
		x = 0;
		y += scale_h;
	}

	//删除画笔
	DeleteObject(whitePenLeft);
	DeleteObject(whitePenRight);
	DeleteObject(redPenLeft);
	DeleteObject(redPenRight);
}

//在主缓（*pbuffDC）上绘制一个方块（*prc）
void MapCompareView::drawrect(CRect *prc,CDC *pbuffDC)
{
	//用于绘制一个空心矩形
	pbuffDC->MoveTo(prc->left,prc->top);
	pbuffDC->LineTo(prc->right,prc->top);
	pbuffDC->LineTo(prc->right,prc->bottom);
	pbuffDC->LineTo(prc->left,prc->bottom);
	pbuffDC->LineTo(prc->left,prc->top);
}

void MapCompareView::draw_edit_area(void)
{
	int a = m_ptAreaTopLeft.x;//左上角的格子在地图矩阵中的坐标
	int b = m_ptAreaTopLeft.y;
	int x = 0;//地图格在屏幕上所占据的矩形左上角的坐标
	int y = 0;
	int i, m, n;
	int scaled_w = TILE_W;
	int scaled_h = TILE_H;
	GetGridScale(&scaled_w, &scaled_h);
	int w_size = EDIT_AREA_W / scaled_w;
	int h_size = EDIT_AREA_W / scaled_h;
	for (m = 0; m < h_size; m++)
	{
		for (n = 0; n < w_size; n++)
		{
			if ((a < MAP_GRID_W) &&
				(a >= 0)         &&
				(b < MAP_GRID_H) &&
				(b >= 0))
			{
				i = (ReadMapLeft(a,b));
				draw_map_ceil(i,x,y,scaled_w,scaled_h,&m_LeftBuffDC);
				i = (ReadMapRight(a,b));
				draw_map_ceil(i,x,y,scaled_w,scaled_h,&m_RightBuffDC);
			}
			a ++;
			x += scaled_w;
		}
		a = m_ptAreaTopLeft.x;
		b ++;
		x = 0;
		y += scaled_h;
	}
}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
//用于清空历史数据，并使undo_st指示失效。
void MapCompareView::ResetBackup()
{
	//复位剪切板队列指针
	m_bkup_counter=0;
	m_bkup_top=0;
}

//向地图矩阵写入一个数据
void MapCompareView::save(enum_left_or_right lor,int value,int m_x_value,int m_y_value)
{
	//将当前操作写入undo队列。是Write函数的子函数
	m_bkup_counter=((m_bkup_counter==CAPACITY)?m_bkup_counter:(++m_bkup_counter));//counter最大值等于CAPACITY
	m_bkup_top=((m_bkup_top==(CAPACITY-1))?0:(++m_bkup_top));
	backup[m_bkup_top][0]=lor;//写入用于undo操作的队列
	backup[m_bkup_top][1]=value;
	backup[m_bkup_top][2]=m_x_value;
	backup[m_bkup_top][3]=m_y_value;
}

//从地图矩阵读出一个数据
BOOL MapCompareView::Load(int* lor,int *pvalue,int *pm_x_value,int *pm_y_value)
{
	//从undo队列中读出一个数据。如果读出失败，将返回FALSE。是Undo函数的子函数
	if(m_bkup_counter>0){
		*lor=backup[m_bkup_top][0];
		*pvalue=backup[m_bkup_top][1];
		*pm_x_value=backup[m_bkup_top][2];
		*pm_y_value=backup[m_bkup_top][3];
		m_bkup_top=((m_bkup_top==0)?(CAPACITY-1):(--m_bkup_top));
		m_bkup_counter--;
		if(m_bkup_counter==0){
			undo_st=FALSE;
		}
		else{
			undo_st=TRUE;
		}
		return TRUE;
	}
	else{
		return FALSE;
	}
}

//执行undo操作的函数
void MapCompareView::Undo(void)
{
	//执行undo操作
	int lor;
	int z;//地图单元格数值
	int x;//反写入的坐标
	int y;
	if (Load(&lor, &z, &x, &y)) {
		switch (lor) {
		case LEFT:
			if (z != 0) {
				map_left[x][y][0]=(char)(z/100);//将undo队列读出的内容写回地图
				map_left[x][y][1]=(char)(z%100);
			}
			else{
				map_left[x][y][0]=0;//将0写入地图
				map_left[x][y][1]=0;
			}
			break;
		case RIGHT:
			if (z != 0) {
				map_right[x][y][0]=(char)(z/100);//将undo队列读出的内容写回地图
				map_right[x][y][1]=(char)(z%100);
			}
			else{
				map_right[x][y][0]=0;//将0写入地图
				map_right[x][y][1]=0;
			}
			break;	
		}
	}
}

//从地图矩阵x,y的位置读出一个数值
int MapCompareView::ReadMapLeft(int x, int y)
{
	if((x>MAP_GRID_W-1)||(x<0)||(y>MAP_GRID_H-1)||(y<0)){
		MessageBox("(Error-001)读取地图文件时输入的位置编号超出了地图尺寸",NULL,MB_OK);
		PostQuitMessage(0);
	}
	int z=(((int)map_left[x][y][0]*100)+(int)(map_left[x][y][1]));
	return(z);
}

//向地图矩阵x,y的位置写入z
void MapCompareView::WriteMapLeft(int x, int y, int z)
{
	if ((x>MAP_GRID_W-1) ||
		(x<0) ||
		(y>MAP_GRID_H-1) ||
		(y<0) ||
		(z>MAP_TILES_NUM_MAX) ||
		(z<0)) {
		MessageBox("(Error-002)写入地图文件时输入的位置编号超出了地图尺寸",NULL,MB_OK);
		PostQuitMessage(0);
	}
	//旧数据写入undo序列
	save(LEFT, (ReadMapLeft(x,y)),x,y);

	if(z!=0){
		map_left[x][y][0]=(char)(z / 100);
		map_left[x][y][1]=(char)(z % 100);
	}
	else{
		map_left[x][y][0]=0;
		map_left[x][y][1]=0;
	}
	m_bLeftModified = TRUE;
}

//从地图矩阵x,y的位置读出一个数值
int MapCompareView::ReadMapRight(int x, int y)
{
	if((x>MAP_GRID_W-1)||(x<0)||(y>MAP_GRID_H-1)||(y<0)){
		MessageBox("(Error-001)读取地图文件时输入的位置编号超出了地图尺寸",NULL,MB_OK);
		PostQuitMessage(0);
	}
	int z=(((int)map_right[x][y][0]*100)+(int)(map_right[x][y][1]));
	return(z);
}

//向地图矩阵x,y的位置写入z
void MapCompareView::WriteMapRight(int x, int y, int z)
{
	if ((x>MAP_GRID_W-1) ||
		(x<0) ||
		(y>MAP_GRID_H-1) ||
		(y<0) ||
		(z>MAP_TILES_NUM_MAX) ||
		(z<0)) {
		MessageBox("(Error-002)写入地图文件时输入的位置编号超出了地图尺寸",NULL,MB_OK);
		PostQuitMessage(0);
	}
	//旧数据写入undo序列
	save(RIGHT, (ReadMapLeft(x,y)),x,y);

	if(z!=0){
		map_right[x][y][0]=(char)(z / 100);
		map_right[x][y][1]=(char)(z % 100);
	}
	else{
		map_right[x][y][0]=0;
		map_right[x][y][1]=0;
	}
	m_bRightModified = TRUE;
}

void MapCompareView::SaveLeftFile(void)
{
	int i, j;
	CFile file(m_sLeftFileName, CFile::modeCreate | CFile::modeWrite);
	CArchive ar(&file, CArchive::store);
	for (i=0;i<MAP_GRID_H;i++) {
		for (j=0;j<MAP_GRID_W;j++) {
			ar<<map_left[i][j][0]<<map_left[i][j][1];
		}
	}
	m_bLeftModified = FALSE;
}
void MapCompareView::LoadLeftFile(void)
{
	if (m_sLeftFileName.IsEmpty()) {
		return;
	}

	int i, j;
	CFile file(m_sLeftFileName, CFile::modeRead);
	CArchive ar(&file, CArchive::load);

	BOOL bValueOutOfRange = FALSE;
	for (i=0;i<MAP_GRID_H;i++) {
		for (j=0;j<MAP_GRID_W;j++) {
			ar>>map_left[i][j][0]>>map_left[i][j][1];
			int v=(((int)map_left[i][j][0]*100)+(int)(map_left[i][j][1]));
			if (v<MAP_TILES_ZERO || v>MAP_TILES_NUM_MAX) {
				map_left[i][j][0] = 0;
				map_left[i][j][1] = 0;
				bValueOutOfRange = TRUE;
			}
		}
	}

	if (bValueOutOfRange) {
		MessageBox("事件文件中存在非法值，已用零值填充！", "提示", MB_OK);
	}

	m_bLeftModified = FALSE;
}
void MapCompareView::SaveRightFile(void)
{
	int i, j;
	CFile file(m_sRightFileName, CFile::modeCreate | CFile::modeWrite);
	CArchive ar(&file, CArchive::store);
	for (i=0;i<MAP_GRID_H;i++) {
		for (j=0;j<MAP_GRID_W;j++) {
			ar<<map_right[i][j][0]<<map_right[i][j][1];
		}
	}
	m_bRightModified = FALSE;
}
void MapCompareView::LoadRightFile(void)
{
	if (m_sRightFileName.IsEmpty()) {
		return;
	}

	int i, j;
	CFile file(m_sRightFileName, CFile::modeRead);
	CArchive ar(&file, CArchive::load);

	BOOL bValueOutOfRange = FALSE;
	for (i=0;i<MAP_GRID_H;i++) {
		for (j=0;j<MAP_GRID_W;j++) {
			ar>>map_right[i][j][0]>>map_right[i][j][1];
			int v=(((int)map_right[i][j][0]*100)+(int)(map_right[i][j][1]));
			if (v<MAP_TILES_ZERO || v>MAP_TILES_NUM_MAX) {
				map_right[i][j][0] = 0;
				map_right[i][j][1] = 0;
				bValueOutOfRange = TRUE;
			}
		}
	}

	if (bValueOutOfRange) {
		MessageBox("事件文件中存在非法值，已用零值填充！", "提示", MB_OK);
	}

	m_bRightModified = FALSE;
}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
void MapCompareView::SetMapCell(enum_left_or_right lor, int x, int y, int value)
{
	if ((x < 0) || (y < 0) || (x > EDIT_AREA_W) || (y > EDIT_AREA_H)) {
		return;
	}

	int X = (m_ptAreaTopLeft.x - 10 + x / 32);
	int Y = (m_ptAreaTopLeft.y - 10 + y / 32);

	if ((X > MAP_GRID_W-1) || (X < 0) || (Y > MAP_GRID_H-1) || (Y < 0)) {
		return;
	}

	switch (lor) {
	case LEFT:
		if (ReadMapLeft (X, Y) != value) {
			WriteMapLeft (X, Y, value);
		}
		break;
	case RIGHT:
		if (ReadMapRight (X, Y) != value) {
			WriteMapRight (X, Y, value);
		}
		break;
	}
	displaymap();
}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

// MapCompareView

MapCompareView::MapCompareView()
{
	m_nMapScale = SCALE1;
	m_bLButtonDown = FALSE;
	m_sLeftFileName = "";
	m_bLeftModified = FALSE;
	m_sRightFileName = "";
	m_bRightModified = FALSE;

	//设置为右下角，避免鼠标点一下就选中
	m_rcMouseSelectArea = CRect(MAP_GRID_W,MAP_GRID_H,MAP_GRID_W,MAP_GRID_H);

	int x, y;
	for (x = 0; x < MAP_GRID_W; x++) {
		for (y = 0; y < MAP_GRID_H; y++) {
			map_diff[x][y] = 0;
		}
	}

	ZeroMemory(map_left, sizeof(map_left));
	ZeroMemory(map_right, sizeof(map_right));
	ZeroMemory(map_diff, sizeof(map_diff));
}

MapCompareView::~MapCompareView()
{
}


BEGIN_MESSAGE_MAP(MapCompareView, CWnd)
	ON_WM_PAINT()
	ON_COMMAND(ID_OPEN_LEFT, &MapCompareView::OnOpenLeft)
	ON_COMMAND(ID_OPEN_RIGHT, &MapCompareView::OnOpenRight)
	ON_COMMAND(ID_SAVE_LEFT, &MapCompareView::OnSaveLeft)
	ON_COMMAND(ID_SAVE_RIGHT, &MapCompareView::OnSaveRight)
	ON_UPDATE_COMMAND_UI(ID_SAVE_LEFT, &MapCompareView::OnUpdateSaveLeft)
	ON_UPDATE_COMMAND_UI(ID_SAVE_RIGHT, &MapCompareView::OnUpdateSaveRight)
	ON_COMMAND(ID_SCALE1, &MapCompareView::OnScale1)
	ON_COMMAND(ID_SCALE2, &MapCompareView::OnScale2)
	ON_COMMAND(ID_SCALE4, &MapCompareView::OnScale4)
	ON_COMMAND(ID_SCALE8, &MapCompareView::OnScale8)
	ON_UPDATE_COMMAND_UI(ID_SCALE1, &MapCompareView::OnUpdateScale1)
	ON_UPDATE_COMMAND_UI(ID_SCALE2, &MapCompareView::OnUpdateScale2)
	ON_UPDATE_COMMAND_UI(ID_SCALE4, &MapCompareView::OnUpdateScale4)
	ON_UPDATE_COMMAND_UI(ID_SCALE8, &MapCompareView::OnUpdateScale8)
	ON_COMMAND(ID_COPY_TO_LEFT, &MapCompareView::OnCopyToLeft)
	ON_COMMAND(ID_COPY_TO_RIGHT, &MapCompareView::OnCopyToRight)
	ON_UPDATE_COMMAND_UI(ID_COPY_TO_LEFT, &MapCompareView::OnUpdateCopyToLeft)
	ON_UPDATE_COMMAND_UI(ID_COPY_TO_RIGHT, &MapCompareView::OnUpdateCopyToRight)
	ON_COMMAND(ID_EDIT_UNDO, &MapCompareView::OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, &MapCompareView::OnUpdateEditUndo)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_CREATE()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_COMMAND(ID_UP, &MapCompareView::OnUp)
	ON_COMMAND(ID_DOWN, &MapCompareView::OnDown)
	ON_COMMAND(ID_LEFT, &MapCompareView::OnLeft)
	ON_COMMAND(ID_RIGHT, &MapCompareView::OnRight)
END_MESSAGE_MAP()



// MapCompareView 消息处理程序

BOOL MapCompareView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	TCHAR path[MAX_PATH];
	strncpy(path, theApp.m_pszHelpFilePath, MAX_PATH);
	int	j=strlen(path);
	for(;path[j]!='\\';j--);
	path[j+1]='\0';
	CString sModelPath = path;
	CString sFileMapTile = sModelPath + "tilemap.bmp";
	if (! PathFileExists(sFileMapTile)) {
		MessageBox("应用程序目录下未发现 tilemap.bmp 文件！");
		return FALSE;
	}
	MapTile=(HBITMAP)LoadImage(NULL,sFileMapTile,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//地图单元格位图
	run_once = TRUE;//保证OnDraw函数中的初始化段落只执行一次

	m_ViewFont.CreateFont(16, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	return TRUE;
}

void MapCompareView::OnPaint() 
{
	CPaintDC dc(this); // 用于绘制的设备上下文
	
	if (run_once) {//程序第一次运行所需的操作

		LeftBuff.CreateCompatibleBitmap(&dc,EDIT_AREA_W,EDIT_AREA_H);
		RightBuff.CreateCompatibleBitmap(&dc,EDIT_AREA_W,EDIT_AREA_H);

		m_TileDC.CreateCompatibleDC(&dc);
		m_TileDC.SelectObject(MapTile);
		m_LeftBuffDC.CreateCompatibleDC(&dc);
		m_LeftBuffDC.SelectObject(&LeftBuff);
		m_RightBuffDC.CreateCompatibleDC(&dc);
		m_RightBuffDC.SelectObject(&RightBuff);

		ResetBackup();
		undo_st=FALSE;//复位剪切板及其参数
		run_once = FALSE;
	}
	displaymap();//用于重绘界面
}

void MapCompareView::OnOpenLeft()
{
	if (! m_sLeftFileName.IsEmpty() && m_bLeftModified) {
		if (IDYES == MessageBox("是否保存当前文件？", "提示", MB_YESNO)) {
			SaveLeftFile();
		}
	}

	//选择存储文件的位置
	CString filter = "文件 (*.map;*.npc)|*.map;*.npc||";	//文件过滤的类型
	CFileDialog fileDlg(true, "map", NULL, OFN_HIDEREADONLY|OFN_READONLY, filter, NULL);
	if (! m_sRightFileName.IsEmpty()) {
		fileDlg.GetOFN().lpstrInitialDir = m_sRightFileName;//参考对侧文件的路径
	}
	if (IDOK == fileDlg.DoModal()) {
		m_sLeftFileName = fileDlg.GetPathName();
		LoadLeftFile();
		// 打开文件成功，如果对侧文件存在，则重新建立比较矩阵
		if (! m_sRightFileName.IsEmpty()) {
			Compare();
		}
		RenewTitle();
		displaymap();
	}
	else {
		return;
	}
}

void MapCompareView::OnOpenRight()
{
	if (! m_sRightFileName.IsEmpty() && m_bRightModified) {
		if (IDYES == MessageBox("是否保存当前文件？", "提示", MB_YESNO)) {
			SaveLeftFile();
		}
	}

	//选择存储文件的位置
	CString filter = "文件 (*.map;*.npc)|*.map;*.npc||";	//文件过滤的类型
	CFileDialog fileDlg(true, "map", NULL, OFN_HIDEREADONLY|OFN_READONLY, filter, NULL);
	if (! m_sLeftFileName.IsEmpty()) {
		fileDlg.GetOFN().lpstrInitialDir = m_sLeftFileName;//参考对侧文件的路径
	}
	if (IDOK == fileDlg.DoModal()) {
		m_sRightFileName = fileDlg.GetPathName();
		LoadRightFile();
		// 打开文件成功，如果对侧文件存在，则重新建立比较矩阵
		if (! m_sLeftFileName.IsEmpty()) {
			Compare();
		}
		RenewTitle();
		displaymap();
	}
	else {
		return;
	}
}

void MapCompareView::Compare(void)
{
	int x, y;
	for (x = 0; x < MAP_GRID_W; x++) {
		for (y = 0; y < MAP_GRID_H; y++) {
			if (map_left[x][y][1] == map_right[x][y][1] &&
				map_left[x][y][0] == map_right[x][y][0])
			{
				map_diff[x][y] = 0;
			}
			else {
				map_diff[x][y] = 1;
			}
		}
	}
}

void MapCompareView::RenewTitle(void)
{
	CString sLeftName = "";
	CString sRightName = "";
	if (! m_sLeftFileName.IsEmpty()) {
		sLeftName = GetFileName(m_sLeftFileName);
	}
	if (! m_sRightFileName.IsEmpty()) {
		sRightName = GetFileName(m_sRightFileName);
	}
	CString sTitle = "地图比较 - 左：" + sLeftName + " ; 右：" + sRightName;
	theApp.GetMainWnd()->SetWindowText(sTitle);
}

void MapCompareView::OnSaveLeft()
{
	SaveLeftFile();
}

void MapCompareView::OnSaveRight()
{
	SaveRightFile();
}

void MapCompareView::OnUpdateSaveLeft(CCmdUI *pCmdUI)
{
	if (! m_sLeftFileName.IsEmpty() && m_bLeftModified) {
		pCmdUI->Enable(1);
	}
	else {
		pCmdUI->Enable(0);
	}
}
void MapCompareView::OnUpdateSaveRight(CCmdUI *pCmdUI)
{
	if (! m_sRightFileName.IsEmpty() && m_bRightModified) {
		pCmdUI->Enable(1);
	}
	else {
		pCmdUI->Enable(0);
	}
}
void MapCompareView::OnScale1()
{
	m_nMapScale = SCALE1;
	displaymap();
}
void MapCompareView::OnScale2()
{
	m_nMapScale = SCALE2;
	displaymap();
}
void MapCompareView::OnScale4()
{
	m_nMapScale = SCALE4;
	displaymap();
}
void MapCompareView::OnScale8()
{
	m_nMapScale = SCALE8;
	displaymap();
}
void MapCompareView::OnUpdateScale1(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(((m_nMapScale == SCALE1))?1:0);
}
void MapCompareView::OnUpdateScale2(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(((m_nMapScale == SCALE2))?1:0);
}
void MapCompareView::OnUpdateScale4(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(((m_nMapScale == SCALE4))?1:0);
}
void MapCompareView::OnUpdateScale8(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(((m_nMapScale == SCALE8))?1:0);
}
void MapCompareView::OnCopyToLeft()
{
	int x = m_rcSelectedGridArea.left;
	int y = m_rcSelectedGridArea.top;
	int r = m_rcSelectedGridArea.right;
	int b = m_rcSelectedGridArea.bottom;
	if (x >= 0 &&
		y >= 0 &&
		r < MAP_GRID_W &&
		b < MAP_GRID_H &&
		m_rcSelectedGridArea.Width()  >= 0 &&
		m_rcSelectedGridArea.Height() >= 0 &&
		! m_sLeftFileName.IsEmpty() &&
		! m_sRightFileName.IsEmpty())
	{
		for (; y <= b; y++) {
			for (x = m_rcSelectedGridArea.left; x <= r; x++) {
				WriteMapLeft(x, y, ReadMapRight(x, y));
			}
		}
	}
	Compare();//重新比较
	displaymap();
}
void MapCompareView::OnCopyToRight()
{
	int x = m_rcSelectedGridArea.left;
	int y = m_rcSelectedGridArea.top;
	int r = m_rcSelectedGridArea.right;
	int b = m_rcSelectedGridArea.bottom;
	if (x >= 0 &&
		y >= 0 &&
		r < MAP_GRID_W &&
		b < MAP_GRID_H &&
		m_rcSelectedGridArea.Width()  >= 0 &&
		m_rcSelectedGridArea.Height() >= 0 &&
		! m_sLeftFileName.IsEmpty() &&
		! m_sRightFileName.IsEmpty())
	{
		for (; y <= b; y++) {
			for (x = m_rcSelectedGridArea.left; x <= r; x++) {
				WriteMapRight(x, y, ReadMapLeft(x, y));
			}
		}
	}
	Compare();//重新比较
	displaymap();
}
void MapCompareView::OnUpdateCopyToLeft(CCmdUI *pCmdUI)
{
	if (m_rcSelectedGridArea.left >= 0 &&
		m_rcSelectedGridArea.top >= 0  &&
		m_rcSelectedGridArea.right < MAP_GRID_W  &&
		m_rcSelectedGridArea.bottom < MAP_GRID_H &&
		m_rcSelectedGridArea.Width() >= 0 &&
		m_rcSelectedGridArea.Height() >= 0)
	{
		pCmdUI->Enable(1);
	}
	else {
		pCmdUI->Enable(0);
	}
}
void MapCompareView::OnUpdateCopyToRight(CCmdUI *pCmdUI)
{
	if (m_rcSelectedGridArea.left >= 0 &&
		m_rcSelectedGridArea.top >= 0  &&
		m_rcSelectedGridArea.right < MAP_GRID_W  &&
		m_rcSelectedGridArea.bottom < MAP_GRID_H &&
		m_rcSelectedGridArea.Width() >= 0 &&
		m_rcSelectedGridArea.Height() >= 0)
	{
		pCmdUI->Enable(1);
	}
	else {
		pCmdUI->Enable(0);
	}
}

void MapCompareView::OnEditUndo()
{
	// TODO: 在此添加命令处理程序代码
}

void MapCompareView::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void MapCompareView::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect rcLeft(0, 0, EDIT_AREA_W, EDIT_AREA_H);
	CRect rcRight(RIGHT_EDIT_AREA_X_OFFSET+1, 0,
		RIGHT_EDIT_AREA_X_OFFSET+EDIT_AREA_W, EDIT_AREA_H);

	if (PtInRect(&rcLeft, point)) {
		if (m_sLeftFileName.IsEmpty()) return;
		ClientToScreen(&rcLeft);
		::ClipCursor(&rcLeft);//限制鼠标的移动范围
	}
	else if (PtInRect(&rcRight, point)) {
		if (m_sRightFileName.IsEmpty()) return;
		ClientToScreen(&rcRight);
		::ClipCursor(&rcRight);
	}
	else {
		return;
	}

	m_bLButtonDown = TRUE;
	m_ptDragStart = point;
	CWnd::OnLButtonDown(nFlags, point);
}

void MapCompareView::OnLButtonUp(UINT nFlags, CPoint point)
{
	::ClipCursor(NULL);
	if (! m_bLButtonDown) return;

	m_bLButtonDown = FALSE;

	//根据鼠标框选范围确定地图矩阵的选中范围
	int scale_w = TILE_W;
	int scale_h = TILE_H;
	GetGridScale(&scale_w, &scale_h);

	//如果是在右边窗口圈选的，则需要挪动一下选区位置
	if (m_rcMouseSelectArea.left > RIGHT_EDIT_AREA_X_OFFSET) {
		m_rcMouseSelectArea.OffsetRect(-RIGHT_EDIT_AREA_X_OFFSET, 0);
	}

	m_rcSelectedGridArea.left = m_ptAreaTopLeft.x + m_rcMouseSelectArea.left / scale_w;
	m_rcSelectedGridArea.top  = m_ptAreaTopLeft.y + m_rcMouseSelectArea.top  / scale_h;
	m_rcSelectedGridArea.right  = m_rcSelectedGridArea.left + m_rcMouseSelectArea.Width() / scale_w;
	m_rcSelectedGridArea.bottom = m_rcSelectedGridArea.top + m_rcMouseSelectArea.Height() / scale_h;
	if (m_rcSelectedGridArea.left < 0) m_rcSelectedGridArea.left = 0;
	if (m_rcSelectedGridArea.top  < 0) m_rcSelectedGridArea.top  = 0;
	if (m_rcSelectedGridArea.left  >=MAP_GRID_W) m_rcSelectedGridArea.left  = MAP_GRID_W-1;
	if (m_rcSelectedGridArea.top   >=MAP_GRID_H) m_rcSelectedGridArea.top   = MAP_GRID_H-1;
	if (m_rcSelectedGridArea.right >=MAP_GRID_W) m_rcSelectedGridArea.right = MAP_GRID_W-1;
	if (m_rcSelectedGridArea.bottom>=MAP_GRID_H) m_rcSelectedGridArea.bottom= MAP_GRID_H-1;

	m_rcMouseSelectArea = CRect(0,0,0,0);
	displaymap();
	CWnd::OnLButtonUp(nFlags, point);
}

void MapCompareView::OnRButtonUp(UINT nFlags, CPoint point)
{
	if (m_bLButtonDown) return;//左键拖动期间，不处理此事件

	//判断鼠标右键落下的位置，尽量保持鼠标指向的格子位置不变
	CRect rcRight(RIGHT_EDIT_AREA_X_OFFSET, 0,
		RIGHT_EDIT_AREA_X_OFFSET+EDIT_AREA_W, EDIT_AREA_H);

	if (PtInRect(&rcRight, point)) {
		point.x -= RIGHT_EDIT_AREA_X_OFFSET;
	}
	int scale_w;
	int scale_h;
	GetGridScale(&scale_w, &scale_h);
	int point_grid_x = m_ptAreaTopLeft.x + point.x / scale_w;
	int point_grid_y = m_ptAreaTopLeft.y + point.y / scale_h;
	
	m_nMapScale ++;
	if (m_nMapScale > SCALE8) {
		m_nMapScale = SCALE1;
	}

	//改变比例之后，尽量保持鼠标指向的格子位置不变
	GetGridScale(&scale_w, &scale_h);
	m_ptAreaTopLeft.x = point_grid_x - point.x / scale_w;
	m_ptAreaTopLeft.y = point_grid_y - point.y / scale_h;

	if (m_ptAreaTopLeft.x < 0) m_ptAreaTopLeft.x = 0;
	if (m_ptAreaTopLeft.x > MAP_GRID_W-1) m_ptAreaTopLeft.x = MAP_GRID_W-1;
	if (m_ptAreaTopLeft.y < 0) m_ptAreaTopLeft.y = 0;
	if (m_ptAreaTopLeft.y > MAP_GRID_H-1) m_ptAreaTopLeft.y = MAP_GRID_H-1;

	m_scoLeftAreaH.SetScrollPos(m_ptAreaTopLeft.x);
	m_scoLeftAreaV.SetScrollPos(m_ptAreaTopLeft.y);
	m_scoRightAreaH.SetScrollPos(m_ptAreaTopLeft.x);
	m_scoRightAreaV.SetScrollPos(m_ptAreaTopLeft.y);

	displaymap();
	CWnd::OnRButtonUp(nFlags, point);
}

void MapCompareView::OnMouseMove(UINT nFlags, CPoint point)
{
	if (m_bLButtonDown) {
		m_rcMouseSelectArea.left   = min(m_ptDragStart.x, point.x);
		m_rcMouseSelectArea.right  = max(m_ptDragStart.x, point.x);
		m_rcMouseSelectArea.top    = min(m_ptDragStart.y, point.y);
		m_rcMouseSelectArea.bottom = max(m_ptDragStart.y, point.y);
		displaymap();
	}
	CWnd::OnMouseMove(nFlags, point);
}

int MapCompareView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// 重要 ！！！下列代码禁用中文输入法
	// 如果不禁用输入法，快捷键ASDF默认输入中文
	DisableIME (this->GetSafeHwnd());

	CRect rc1(0, EDIT_AREA_H + 1, EDIT_AREA_W, EDIT_AREA_H + SCROLL_BAR_W);
	if (-1 == m_scoLeftAreaH.Create(SBS_HORZ | WS_CHILD, rc1, this, IDR_LEFT_AREA_SCROLL_H))
		return -1;
	CRect rc2(EDIT_AREA_W + 1, 0, EDIT_AREA_W + SCROLL_BAR_W, EDIT_AREA_H);
	if (-1 == m_scoLeftAreaV.Create(SBS_VERT | WS_CHILD, rc2, this, IDR_LEFT_AREA_SCROLL_V))
		return -1;
	CRect rc3(RIGHT_EDIT_AREA_X_OFFSET, EDIT_AREA_H + 1,
		RIGHT_EDIT_AREA_X_OFFSET +EDIT_AREA_W, EDIT_AREA_H + SCROLL_BAR_W);
	if (-1 == m_scoRightAreaH.Create(SBS_HORZ | WS_CHILD, rc3, this, IDR_LEFT_AREA_SCROLL_H))
		return -1;
	CRect rc4(RIGHT_EDIT_AREA_X_OFFSET + EDIT_AREA_W + 1, 0,
		RIGHT_EDIT_AREA_X_OFFSET + EDIT_AREA_W + SCROLL_BAR_W, EDIT_AREA_H);
	if (-1 == m_scoRightAreaV.Create(SBS_VERT | WS_CHILD, rc4, this, IDR_LEFT_AREA_SCROLL_V))
		return -1;

	m_scoLeftAreaH.ShowScrollBar(TRUE);
	m_scoLeftAreaV.ShowScrollBar(TRUE);
	m_scoRightAreaH.ShowScrollBar(TRUE);
	m_scoRightAreaV.ShowScrollBar(TRUE);

	m_scoLeftAreaH.SetScrollRange(0, MAP_GRID_W-1);
	m_scoLeftAreaV.SetScrollRange(0, MAP_GRID_H-1);
	m_scoRightAreaH.SetScrollRange(0, MAP_GRID_W-1);
	m_scoRightAreaV.SetScrollRange(0, MAP_GRID_H-1);

	m_scoLeftAreaH.SetScrollPos(m_ptAreaTopLeft.x);
	m_scoLeftAreaV.SetScrollPos(m_ptAreaTopLeft.y);
	m_scoRightAreaH.SetScrollPos(m_ptAreaTopLeft.x);
	m_scoRightAreaV.SetScrollPos(m_ptAreaTopLeft.y);

	return 0;
}


void MapCompareView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (pScrollBar == &m_scoLeftAreaV ||
		pScrollBar == &m_scoRightAreaV)
	{
		switch (nSBCode) {
		case SB_LINEUP:
			if (m_ptAreaTopLeft.y > 0) {
				m_ptAreaTopLeft.y --;
			}
			else {
				return;
			}
			break;
		case SB_PAGEUP:
			if (m_ptAreaTopLeft.y > 0) {
				m_ptAreaTopLeft.y -= EDIT_PAGE_STEP;
				if (m_ptAreaTopLeft.y < 0) m_ptAreaTopLeft.y = 0;
			}
			else {
				return;
			}
			break;
		case SB_LINEDOWN:
			if (m_ptAreaTopLeft.y < MAP_GRID_H-1) {
				m_ptAreaTopLeft.y ++;
			}
			else {
				return;
			}
			break;
		case SB_PAGEDOWN:
			if (m_ptAreaTopLeft.y < MAP_GRID_H-1) {
				m_ptAreaTopLeft.y += EDIT_PAGE_STEP;
				if (m_ptAreaTopLeft.y > MAP_GRID_H-1) {
					m_ptAreaTopLeft.y = MAP_GRID_H-1;
				}
			}
			else {
				return;
			}
			break;
		case SB_THUMBTRACK:
			if (nPos >= 0 && nPos < MAP_GRID_H) {
				m_ptAreaTopLeft.y = nPos;
			}
			break;
		}

		if (m_ptAreaTopLeft.y >= 0 && m_ptAreaTopLeft.y < MAP_GRID_H) {
			m_scoLeftAreaV.SetScrollPos(m_ptAreaTopLeft.y);
			m_scoRightAreaV.SetScrollPos(m_ptAreaTopLeft.y);
			displaymap();
		}
	}
}


void MapCompareView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (pScrollBar == &m_scoLeftAreaH ||
		pScrollBar == &m_scoRightAreaH)
	{
		switch (nSBCode) {
		case SB_LINELEFT:
			if (m_ptAreaTopLeft.x > 0) {
				m_ptAreaTopLeft.x --;
			}
			else {
				return;
			}
			break;
		case SB_PAGELEFT:
			if (m_ptAreaTopLeft.x > 0) {
				m_ptAreaTopLeft.x -= EDIT_PAGE_STEP;
				if (m_ptAreaTopLeft.x < 0) m_ptAreaTopLeft.x = 0;
			}
			else {
				return;
			}
			break;
		case SB_LINERIGHT:
			if (m_ptAreaTopLeft.x < MAP_GRID_W-1) {
				m_ptAreaTopLeft.x ++;
			}
			else {
				return;
			}
			break;
		case SB_PAGERIGHT:
			if (m_ptAreaTopLeft.x < MAP_GRID_W-1) {
				m_ptAreaTopLeft.x += EDIT_PAGE_STEP;
				if (m_ptAreaTopLeft.x > MAP_GRID_W-1) {
					m_ptAreaTopLeft.x = MAP_GRID_W-1;
				}
			}
			else {
				return;
			}
			break;
		case SB_THUMBTRACK:
			if (nPos >= 0 && nPos < MAP_GRID_W) {
				m_ptAreaTopLeft.x = nPos;
			}
			break;
		}

		if (m_ptAreaTopLeft.x >= 0 && m_ptAreaTopLeft.x < MAP_GRID_W) {
			m_scoLeftAreaH.SetScrollPos(m_ptAreaTopLeft.x);
			m_scoRightAreaH.SetScrollPos(m_ptAreaTopLeft.x);
			displaymap();
		}
	}
}


void MapCompareView::OnUp()
{
	if (m_ptAreaTopLeft.y > 0) {
		m_ptAreaTopLeft.y -= EDIT_PAGE_STEP;
		if (m_ptAreaTopLeft.y < 0) m_ptAreaTopLeft.y = 0;
		m_scoLeftAreaV.SetScrollPos(m_ptAreaTopLeft.y);
		m_scoRightAreaV.SetScrollPos(m_ptAreaTopLeft.y);
	}
}


void MapCompareView::OnDown()
{
	if (m_ptAreaTopLeft.y < MAP_GRID_H-1) {
		m_ptAreaTopLeft.y += EDIT_PAGE_STEP;
		if (m_ptAreaTopLeft.y > MAP_GRID_H-1) {
			m_ptAreaTopLeft.y = MAP_GRID_H-1;
		}
		m_scoLeftAreaV.SetScrollPos(m_ptAreaTopLeft.y);
		m_scoRightAreaV.SetScrollPos(m_ptAreaTopLeft.y);
	}
}


void MapCompareView::OnLeft()
{
	if (m_ptAreaTopLeft.x > 0) {
		m_ptAreaTopLeft.x -= EDIT_PAGE_STEP;
		if (m_ptAreaTopLeft.x < 0) m_ptAreaTopLeft.x = 0;
		m_scoLeftAreaH.SetScrollPos(m_ptAreaTopLeft.x);
		m_scoRightAreaH.SetScrollPos(m_ptAreaTopLeft.x);
	}
}


void MapCompareView::OnRight()
{
	if (m_ptAreaTopLeft.x < MAP_GRID_W-1) {
		m_ptAreaTopLeft.x += EDIT_PAGE_STEP;
		if (m_ptAreaTopLeft.x > MAP_GRID_W-1) {
			m_ptAreaTopLeft.x = MAP_GRID_W-1;
		}
		m_scoLeftAreaH.SetScrollPos(m_ptAreaTopLeft.x);
		m_scoRightAreaH.SetScrollPos(m_ptAreaTopLeft.x);
	}
}
